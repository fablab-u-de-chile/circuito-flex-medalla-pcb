# Pulsera Flex Medalla PCB v1.0

<div align="center">
<img src="img/pulsera_on.jpg/"
     alt="render_iso"
     height="400">
</div>

Corresponde a una placa de circuito electrónica (PCB) semi-flexible.

El propósito de este circuito, es poder explorar la factibilidad de fabricar prototipos de PCBs flexibles y, asmismo, potenciales usos de este tipo de circuitos.

## Componentes
 
La PCB contempla:

- 1 x LED azul
- 1 x resistencia de 200 ohm

## Diseño

[foto]

## Testeo

</div>
<div align="center">
<img src="img/pulsera_work_1.jpg/"
     alt="render_iso"
     height="400">
</div>

## Programación

## Archivos


- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment

Integrantes:

